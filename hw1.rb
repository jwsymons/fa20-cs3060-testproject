#homework 1 for CS 3060



#writes three input lines to file
def function1

    linesFile = File.new("savedLines.txt", "w")

    puts "Enter three lines:\n"

    #get and write 3 lines to the file
    i = 0
    while i < 3 
        line = gets
        linesFile.write(line)
        i += 1
    end

    linesFile.close

end


#checks input file for keywords
def function2

    important = ["CPlusPlus", "Python"]
    extraordinary = ["Ruby", "Scala"]
    unknown_flag = true

    puts "Enter a file name: \n"
    filename = gets

    if File.exists?("#{filename}")

        f = File.new("#{filename.chomp!}", "r")

        f.each do |line|
            if line.include?(important[0]) || line.include?(important[1])
                puts "The file is important."
                unknown_flag = false
                break
            elsif line.include?(extraordinary[0]) || line.include?(extraordinary[1])
                puts "The file is extraordinary."
                unknown_flag = false
                break
            end
        end

        f.close
    end

    #print unknown if other lines did not print
    puts "The file is unknown." if unknown_flag


end


#lists cubes of integers 1 through 50
def function3
    a = 1
    while a <= 50
        puts "The cube of integer #{a} is #{a*a*a}.\n"
        a += 1
    end
end



#flips coin x times where x is input between 10 and 200
def function4
    #get input x between 10 and 200
    begin
        puts "Pick a number between 10 and 200:\n"
        num_flips = gets.chomp!.to_i
    end while num_flips < 10 || num_flips > 200

    flip_arr = []

    #fill array with x coin flips
    for i in 0..(num_flips - 1)
        flip_arr[i] = rand(2)
        i += 1
    end

    #count number of heads/tails
    num_tails = 0
    num_heads = 0
    flip_arr.each{|bit| if bit == 1 then num_heads += 1; else num_tails += 1; end}

    puts "The ratio of heads to tails is #{num_heads} to #{num_tails}."


end


#parses and analyzes txt file
def function5

    wordArr = []

    wordHash = Hash.new(0)

    wordDef = /[\w']+/

    #convert file to array of words
    story = File.new("story.txt", "r")

    story.each do |line|
        lineArr = line.scan(wordDef)
        wordArr.concat(lineArr)
    end

    story.close

    #fill hash with word frequencies
    wordArr.each do |word|
        if wordHash.has_key?(word)
            wordHash[word] += 1
        else
            wordHash[word] = 1
        end
    end

    #find second most frequent word
    firstFrequent = nil
    secondFrequent = nil
    wordHash.keys.each do |key|
        if wordHash[key] > wordHash[firstFrequent]
            secondFrequent = firstFrequent
            firstFrequent = key
        elsif wordHash[key] > wordHash[secondFrequent]
            secondFrequent = key
        end
    end

    #find all words that start with s
    num_s_words = 0
    wordArr.each do |word|
        num_s_words += 1 if word.match(/\bs[\w']+\b/)
    end

    #find number of DISTINCT s words
    num_distinct_s_words = 0
    wordHash.keys.each do |word|
        num_distinct_s_words += 1 if word.match(/\bs[\w']+\b/)
    end

    #print out all necessary info
    puts "The total number of words in the story is #{wordArr.size}."
    puts "The number of distinct words is #{wordHash.size}."
    puts "The second most frequent word is \"#{secondFrequent}\", which appears #{wordHash[secondFrequent]} times."
    puts "The total number of words that start with \"s\" is #{num_s_words}."
    puts "The number of distinct words that start with \"s\" is #{num_distinct_s_words}."

    

end


function1
function2
function3
function4
function5
